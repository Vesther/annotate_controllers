namespace :annotate do
  desc "Annotates controllers with their respective routes and route names"
  task controllers: :environment do
    filter_routes = lambda do |routes|
      controllers_to_filter = [
        "rails/info",
        "rails/mailers",
        "rails/welcome"
      ]

      filtered_routes = routes
      filtered_routes.reject! do |route|
        controller_name = route.defaults[:controller].to_s
        controller_name.blank? || controllers_to_filter.include?(controller_name) || route.name.to_s == "root"
      end

      filtered_routes.map do |route|
        {
          name: route.name.to_s,
          verb: route.verb,
          path: route.path.spec.to_s.chomp("(.:format)"),
          controller: route.defaults[:controller].to_s,
          action: route.defaults[:action].to_s
        }
      end
    end

    all_routes = filter_routes.call(Rails.application.routes.routes.to_a)
    routes_by_controller = all_routes.group_by { |route| route[:controller] }

    annotated_actions_counter = 0

    routes_by_controller.each do |controller, routes|
      path = Rails.root.join("app", "controllers", "#{controller}_controller.rb")
      puts "Annotating #{path}"
      file = File.read(path)

      routes.each do |route|
        search_string = "def #{route[:action]}"
        line_index = file.lines.find_index do |line|
          line.include?(search_string) && line[0...line.index(search_string)] !~ /\S/
        end

        unless line_index
          puts "Couldn't find action: #{route[:action]}, skipping"
          next
        end

        line = file.lines[line_index]
        indent = line[0...line.index(search_string)]

        file = file.lines.insert(line_index, "#{indent}\# #{route[:verb]} #{route[:path]}\n#{route[:name].blank? ? '' : "#{indent}\# #{route[:name]}_path\n"}").join("")
        annotated_actions_counter += 1
      end
      File.write(path, file)
    end

    puts "Annotated #{annotated_actions_counter} actions in #{routes_by_controller.keys.size} files"
  end
end
